using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SoftwareFocusChangeMono : MonoBehaviour
{

    public SoftwareFocusChange m_lastChange;
    public List<SoftwareFocusChange> m_lastChangeList = new List<SoftwareFocusChange>();

    public enum Use { InUpdate, Directly }
    public Use m_useType = Use.Directly;

    [Space]
    public UnityEvent m_onWinMinized;
    public UnityEvent m_onLoseMinized;
    [Space]
    public UnityEvent m_onWinOutOfApp;
    public UnityEvent m_onLostOutOfApp;
    [Space]
    public UnityEvent m_onWinFocus;
    public UnityEvent m_onLoseFocus;
    [Space]
    public UnityEvent m_onWinFocusByMouse;
    public UnityEvent m_onLoseFocusByMouse;


    public void Update()
    {
        if (m_useType == Use.InUpdate) { 
            if (m_lastChangeList.Count > 0)
            {
                for (int i = 0; i < m_lastChangeList.Count; i++)
                {
                    Set(m_lastChangeList[i].GetStateValue(), m_lastChangeList[i].GetCurrentState());
                }
                m_lastChangeList.Clear();
            }
        }
    }

    private void Set(bool active, SoftwareState softwareState)
    {
        switch (softwareState)
        {
            case SoftwareState.Minimazed:
                if (active) m_onWinMinized.Invoke();
                else m_onLoseMinized.Invoke();
                break;
            case SoftwareState.OutOfFocusButThere:

                if (active) m_onWinOutOfApp.Invoke();
                else m_onLostOutOfApp.Invoke();
                break;
            case SoftwareState.Focused:

                if (active) m_onWinFocus.Invoke();
                else m_onLoseFocus.Invoke();
                break;
            case SoftwareState.MouseFocused:

                if (active) m_onWinFocusByMouse.Invoke();
                else m_onLoseFocusByMouse.Invoke();
                break;
            case SoftwareState.CustomByUser:
                break;
            default:
                break;
        }
    }

    private void ChangeHappened(SoftwareFocusChange change)
    {
        m_lastChange = change;
        if (m_useType == Use.InUpdate)
        {

            m_lastChangeList.Add(change);
        }
        else {
            Set(change.GetStateValue(), change.GetCurrentState());
        }
    }


    private void Awake()
    {
        SoftwareFocusChange.AddFocusListener(ChangeHappened);
    }


    private void OnDestroy()
    {

        SoftwareFocusChange.RemoveFocusListener(ChangeHappened);
    }

}


