using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Events;


[System.Serializable]
public class SoftwareFocusChangeEvent : UnityEvent<SoftwareFocusChange> { };
public delegate void SoftwareFocusChangeCall(SoftwareFocusChange change);

[System.Serializable]
public class SoftwareFocusChange
{
    bool m_isOn;
    SoftwareState m_current;

    public bool GetStateValue() { return m_isOn; }
    public SoftwareState GetCurrentState() { return m_current; }


     static SoftwareFocusChange m_lastChange = new SoftwareFocusChange(SoftwareState.CustomByUser, false);
     static SoftwareState m_currentState;
     static SoftwareFocusChangeCall m_changeObserver;

    public static SoftwareFocusChange GetLastChange() { return m_lastChange; }
    public static SoftwareState GetState() { return m_currentState; }
    public static void AddFocusListener(SoftwareFocusChangeCall listener) { m_changeObserver += listener; }
    public static void RemoveFocusListener(SoftwareFocusChangeCall listener) { m_changeObserver -= listener; }

    public SoftwareFocusChange(SoftwareState current, bool stateTrue)
    {
        m_isOn = stateTrue;
        m_current = current;
    }

    public static void PingChange(SoftwareState newState, bool stateTrue) {
        if (m_lastChange.GetCurrentState() == newState && m_lastChange.GetStateValue() == stateTrue)
            return;
        SoftwareFocusChange change = new SoftwareFocusChange(newState, stateTrue);
        m_currentState = newState;
        m_lastChange = change;
        if (m_changeObserver != null)
            m_changeObserver(change);


    }
}
public enum SoftwareState { Minimazed, OutOfFocusButThere, Focused, MouseFocused, CustomByUser }


public class SoftwareFocusStateObserver : MonoBehaviour
{

    [System.Serializable]
    public class SoftStateChange
    {

        public bool m_value;
        public SoftwareState m_state;

        public SoftStateChange(bool value, SoftwareState state)
        {
            m_value = value;
            m_state = state;
        }

        public void Set(bool value)
        {

            if (value != m_value)
            {
                m_value = value;
                SoftwareFocusChange.PingChange(m_state, value);
            }
        }
    }



    public bool m_hasFocus;
    public bool m_isMinized;
    public bool m_isMouseOver;


    public SoftStateChange m_mini          = new SoftStateChange(false, SoftwareState.Minimazed);
    public SoftStateChange m_outOfFocus    = new SoftStateChange(false, SoftwareState.OutOfFocusButThere);
    public SoftStateChange m_focus         = new SoftStateChange(false, SoftwareState.Focused);
    public SoftStateChange m_mouseFocus    = new SoftStateChange(false, SoftwareState.MouseFocused);

   

    private void Awake()
    {

        InvokeRepeating("CheckState", 0, 0.2f);
        m_atAwakePtr = GetActiveWindow();

    }

    public void CheckState()
    {

        
        m_isMouseOver = IsMouseOver();

        m_isMinized = CheckIfIsMinized();

        m_mini.Set(m_isMinized);
        m_outOfFocus.Set(!m_isMinized && !m_hasFocus);
        m_focus.Set( m_hasFocus);
        m_mouseFocus.Set(m_isMouseOver && m_hasFocus);
    }  

    private bool IsMouseOver()
    {
        if (Camera.main == null) {
            return false;
        }

        //MAYBE I SHOULD FIND A WAY THAT IS BASED ON THE REAL POSITION OF THE APP IN THE SCREEN BUT IT NEED TO USE SYS32 ?!
        Vector3 pt = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        return pt.x >= 0f && pt.x <= 1f && pt.y >= 0f && pt.y <= 1f;

    }

    private bool CheckIfIsMinized()
    {
      //  return Screen.width < 50 && Screen.height < 50;
        return IsIconic(m_atAwakePtr);
    }
    private static IntPtr m_atAwakePtr;
    [DllImport("user32.dll")]
    private static extern IntPtr GetActiveWindow();
    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    static extern bool IsIconic(IntPtr hWnd);

   
    private void OnApplicationFocus(bool focus)
    {
        m_hasFocus = focus;

    }

  
}
