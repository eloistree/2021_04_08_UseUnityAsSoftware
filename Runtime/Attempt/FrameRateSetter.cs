﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FrameRateSetter : MonoBehaviour
{
    public int m_choosedFrameRate;
    private int m_choosedFrameRatePrevious;
    void Start()
    {
        SetFrameRateOfTheApp(m_choosedFrameRate);
    }

    public void SetFrameRateOfTheApp(int frame)
    {

        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = frame;
    }
    public void SetFrameRateOfTheApp(string frameAsString)
    {
        
            int i;
            if(int.TryParse(frameAsString,out i))
                SetFrameRateOfTheApp(i);
      
    }

    private void OnValidate()
    {
        CheckForChangeAndApply();
    }

    private void CheckForChangeAndApply()
    {
        if (m_choosedFrameRatePrevious != m_choosedFrameRate)
            SetFrameRateOfTheApp(m_choosedFrameRate);
        m_choosedFrameRatePrevious = m_choosedFrameRate;
    }
}
