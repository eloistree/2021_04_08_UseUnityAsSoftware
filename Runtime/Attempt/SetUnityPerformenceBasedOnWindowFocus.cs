using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetUnityPerformenceBasedOnWindowFocus : MonoBehaviour
{
    public int m_frameWhenUsing = 30;
    public int m_frameWhenNotUsing = 4;
    public int m_applicationMinimiazedFrameRate = 1;


    public void SetFrameRateForWhenUserUsing(string frameAsString)
    {
        int i;
        if (int.TryParse(frameAsString, out i))
        {
            m_frameWhenUsing = i;
        }
    }
    public void SetFrameRateForUserNotUsing(string frameAsString)
    {
        int i;
        if (int.TryParse(frameAsString, out i))
        {
            m_frameWhenNotUsing = i;
        }
    }


    public int minWidth = 128;
    public int minHeight = 64;
    private int previousWidth;
    private int previousHeight;

    private void Awake()
    {

        previousWidth = Screen.width;
        previousHeight = Screen.height;
    }

    public void SetSizeToMin() {

        previousWidth = Screen.width;
        previousHeight = Screen.height;
        Screen.SetResolution(minWidth, minHeight,
            false);
    }
    public void SetSizeToMax() {
 
        Screen.SetResolution(previousWidth, previousHeight,
              false);
    }

    public void SetFrameAsMinimum()
    {
        SetFrameRate(1);
    }
    public void SetFrameAs60FPS()
    {
        SetFrameRate(60);
    }
    public void SetFrameAs30FPS()
    {
        SetFrameRate(30);
    }
    public void SetFrameAsUserUsing()
    {
        SetFrameRate(m_frameWhenUsing);
    }
    public void SetFrameAsUserNotUsing()
    {
        SetFrameRate(m_frameWhenNotUsing);
    }

    public void SetFrameRate(int frame)
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = frame;
    }
}
