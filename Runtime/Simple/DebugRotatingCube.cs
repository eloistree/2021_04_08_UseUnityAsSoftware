using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugRotatingCube : MonoBehaviour
{
    public float m_rotationSpeed= 60f;
    public Vector3 m_direction= Vector3.one;
    public Space m_spaceType = Space.World;
    void Update()
    {
        transform.Rotate(m_direction, m_rotationSpeed * Time.deltaTime, m_spaceType) ;
    }
}
