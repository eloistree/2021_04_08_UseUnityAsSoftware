using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Demo_CameraColorChange : MonoBehaviour
{
    public Camera m_target;
    public Color m_fullFocus= Color.green;
    public Color m_midFocus= Color.yellow;
    public Color m_lowFocus = Color.red;
    public Color m_noFocus = Color.black;

    public void SetFullFocus() { m_target.backgroundColor = m_fullFocus; }
    public void SetMidFocus() { m_target.backgroundColor = m_midFocus; }
    public void SetLowFocus() { m_target.backgroundColor = m_lowFocus; }
    public void SetNoFocus() { m_target.backgroundColor = m_noFocus; }
}
